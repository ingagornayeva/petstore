var newman = require('newman'); 
newman.run({
    collection: require('./PetStore.postman_collection.json'),
    environment: require('./Test.postman_environment.json'),
    insecure: true, 
    timeout: 180000  
}).on('start', function (err, args) { 
    console.log('running a collection...');
}).on('done', function (err, summary) {
    if (err || summary.error) {
        console.error('collection run encountered an error.');
    }
    else {
        console.log('collection run completed.');
    }
});
