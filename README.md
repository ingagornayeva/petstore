
<p align="center">
  <img src="https://media.giphy.com/media/7NoNw4pMNTvgc/giphy.gif" alt="Testing animation">
</p>

<h3 align="center">All my tests, ready for takeoff!</h3>


## Description
This repository contains all of the Postman tests for my project and is set up to run the tests using Newman, a command-line tool for running Postman tests. The tests are organized into collections that correspond to different parts of the system.
## Getting starteed
Make sure you have Node.js and Newman installed.
Clone this repository to your local machine using git clone.
Open a terminal window and navigate to the cloned repository folder.
Install the required dependencies using npm install.
## Running tests with newman
Running Tests with Newman
In the terminal, run the command node index.js.
The test results will be displayed in the terminal window.
## Test cases
https://docs.google.com/spreadsheets/d/1Ss1Ao_eQ5KZxnp8ml8VPRBtGYuZ1w1G4LV60Xhu3Jso/edit#gid=0
made by QA Sisters community in telegram
## Negative check list
https://docs.google.com/spreadsheets/d/1Ss1Ao_eQ5KZxnp8ml8VPRBtGYuZ1w1G4LV60Xhu3Jso/edit#gid=265946286
made by QA Sisters community in telegram

